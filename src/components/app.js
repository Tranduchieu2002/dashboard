import html from "../core.js"
import sideBar from "./sideBar.js";
const components = {
   sideBar: function(callback) {
   return callback()
   },
   header: function (callback) {
     return callback();
   },
   container: function (callback) {
     return callback();
   },
   todoList: function (callback) {
     return callback();
   },
   footer: function (callback) {
     return callback();
   },
 };
export default function app () {
    return html`${sideBar()}`
 }