import html from "../core.js";
import navItems from "./navItems.js";
const imgUser = [
  "https://i.pinimg.com/originals/df/47/bd/df47bd458bf9c7b5e55a8c248cacde0e.gif",
];
const icons = [
  ' <svg  xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-4 " viewBox="0 0 20 20" fill="currentColor" > <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z" /><path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z" /> </svg>',
  '<svg xmlns="http://www.w3.org/2000/svg"  class="h-6 w-6"  fill="none"  viewBox="0 0 24 24"  stroke="currentColor"> <path  stroke-linecap="round"  stroke-linejoin="round" stroke-width="2"  d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" /></svg>',
  ' <svg xmlns="http://www.w3.org/2000/svg"   class="h-5 w-5 mr-4 "viewBox="0 0 20 20" fill="currentColor"> <path fill-rule="evenodd" d="M10 2a4 4 0 00-4 4v1H5a1 1 0 00-.994.89l-1 9A1 1 0 004 18h12a1 1 0 00.994-1.11l-1-9A1 1 0 0015 7h-1V6a4 4 0 00-4-4zm2 5V6a2 2 0 10-4 0v1h4zm-6 3a1 1 0 112 0 1 1 0 01-2 0zm7-1a1 1 0 100 2 1 1 0 000-2z"  clip-rule="evenodd"/></svg>  ',
  `<svg
  xmlns="http://www.w3.org/2000/svg"
  class="h-5 w-5 mr-4 "
  viewBox="0 0 20 20"
  fill="currentColor"
>
  <path
    fill-rule="evenodd"
    d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4zm2 6a1 1 0 011-1h6a1 1 0 110 2H7a1 1 0 01-1-1zm1 3a1 1 0 100 2h6a1 1 0 100-2H7z"
    clip-rule="evenodd"
  />
</svg>`,
  ' <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-4" fill="none" viewBox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round"stroke-width="2" d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1" /> </svg>',
  ' <svg xmlns="http://www.w3.org/2000/svg"class="h-5 w-5 " viewBox="0 0 20 20"  fill="currentColor" > <path   d="M8 9a3 3 0 100-6 3 3 0 000 6zM8 11a6 6 0 016 6H2a6 6 0 016-6zM16 7a1 1 0 10-2 0v1h-1a1 1 0 100 2h1v1a1 1 0 102 0v-1h1a1 1 0 100-2h-1V7z"  /> </svg>',
];
const namesNav = [
  "Dashboard",
  "User",
  "Products",
  "Blogs",
  "Login",
  "Register",
];
export default function sideBar() {
  return html`
    <nav class="  inset-0 w-nav  border-r h-full fixed">
      <div class="nav-logo h-20 flex-center">
        <a href="#" class="py-2 pl-5 pr-4"> </a>
      </div>
      <div class="nav-user mt-0 mx-5 mb-10">
        <a href="#" class="py-2  pl-5 pr-4">
          <div
            class="box-profile py-4 px-5 bg-xam flex-center justify-between rounded"
          >
            <img
              class="h-12 w-12"
              src="${imgUser}"
              onerror="this.src='../src/img/errorImg.png'"
              class="bg-red w-14 mt-6 h-14 align-middle"
              alt=""
            />
            <h4
              class="font-medium text-center
                  "
            >
              Tran Hieu
            </h4>
          </div>
        </a>
      </div>
      <div class="nav-menu">
        <ul class="nav-list  ">
          ${namesNav.map((nameNav, index) =>
            navItems(nameNav, icons[index], index)
          )}
        </ul>
      </div>
    </nav>
  `;
}
/*
          <li class="nav-items flex-start ">
            <a
              href="#Dashboard"
              class="js_active py-2 w-full flex-start pl-5 pr-4"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-5 w-5 mr-5 "
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z" />
                <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z" />
              </svg>
              <h5 class="">Dashboard</h5>
            </a>
          </li>
          <li class="nav-items flex-start ">
            <a href="#User" class="js_active py-2 w-full flex-start pl-5 pr-4">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z"
                />
              </svg>
              <h4 class="">User</h4>
            </a>
          </li>
          <li class="nav-items flex-start ">
            <a
              href="#Products"
              class="js_active py-2 w-full  flex-start pl-5 pr-4"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fill-rule="evenodd"
                  d="M10 2a4 4 0 00-4 4v1H5a1 1 0 00-.994.89l-1 9A1 1 0 004 18h12a1 1 0 00.994-1.11l-1-9A1 1 0 0015 7h-1V6a4 4 0 00-4-4zm2 5V6a2 2 0 10-4 0v1h4zm-6 3a1 1 0 112 0 1 1 0 01-2 0zm7-1a1 1 0 100 2 1 1 0 000-2z"
                  clip-rule="evenodd"
                />
              </svg>
              <h4 class="">Products</h4>
            </a>
          </li>
          <li class="nav-items  flex-start  ">
            <a
              href="#Blogs"
              class="js_active py-2  w-full flex-start pl-5 pr-4"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fill-rule="evenodd"
                  d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4zm2 6a1 1 0 011-1h6a1 1 0 110 2H7a1 1 0 01-1-1zm1 3a1 1 0 100 2h6a1 1 0 100-2H7z"
                  clip-rule="evenodd"
                />
              </svg>
              <h4 class="">Blogs</h4>
            </a>
          </li>
          <li class="nav-items  style_nav-items ">
            <a
              href="#Login"
              class="js_active py-2 w-full flex-start pl-5 pr-4   "
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-6 w-6 mr-2"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1"
                />
              </svg>
              <h4 class=" ">Login</h4>
            </a>
          </li>
          <li class="nav-items flex-start ">
            <a
              href="#Register"
              class="js_active py-2 w-full flex-start pl-5 pr-4"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  d="M8 9a3 3 0 100-6 3 3 0 000 6zM8 11a6 6 0 016 6H2a6 6 0 016-6zM16 7a1 1 0 10-2 0v1h-1a1 1 0 100 2h1v1a1 1 0 102 0v-1h1a1 1 0 100-2h-1V7z"
                />
              </svg>
              <h4 class="">Register</h4>
            </a>
          </li>  */
