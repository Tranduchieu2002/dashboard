import html from "../core.js";

export default function navItems(name, icon, index) {
  

  return html`
    <li class="nav-items flex-start "onclick="dispatch('handNav',this)
    ">
      <a href="#${name}" class="js_active py-2 w-full flex-start pl-5 pr-4">
        ${icon}
        <h5 class="">${name}</h5>
      </a>
    </li>
  `;
}
