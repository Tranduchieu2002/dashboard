export default function html([fistValue, ...strings], ...values) {
   return values
     .reduce(
       function (acc, ind) {
         return acc.concat(ind, strings.shift());
       },
       [fistValue]
     )
     .filter((anyValue) => (anyValue && anyValue !== true) || anyValue === 0)
     .join("");
 }