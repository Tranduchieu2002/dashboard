/* import { darkMode } from "../../tailwind.config";
 */
import Html from "./enable.js";
const $$ = document.querySelectorAll.bind(document);
const $ = document.querySelector.bind(document);
var body = document.body; // Safari
var browser = document.documentElement; // chrome ,firefox,...
const html = $("html");
let nav = [...$$(".nav-items")];
let LocalMODE = "MODE";

for (let index = 0; index < nav.length; index++) {
  let j = 0;
  nav[index].addEventListener("click", function () {
    let current = $$(".style_nav-items");
    if (current.length > 0) {
      current[0].className = current[0].className.replace(
        "style_nav-items",
        ""
      );
    }
    this.className += "style_nav-items";
  });
}
const saveLocal = {
  get(key) {
    return JSON.parse(localStorage.getItem(key)) ?? [];
  },
  set(key, values) {
    return localStorage.setItem(key, JSON.stringify(values));
  },
};
const switchMode = $("#switch-id");
let darkMode = false;

switchMode.onclick = function () {
  darkMode = !darkMode;
  saveLocal.set("MODE", darkMode);
  html.classList.toggle("dark");
  /* !switchMode.checked
    ? (switchMode.checked = darkMode) && html.classList.add("dark")
    : (switchMode.checked = !darkMode) && html.classList.remove("dark"); */
  if (switchMode.checked) {
    darkMode = true;
  } else {
    darkMode = false;
    localStorage.setItem("MODE", darkMode);
  }
};
console.log(switchMode.checked);

if (saveLocal.get("MODE")) {
  html.classList.add("dark");
  switchMode.checked = true;
  console.log(darkMode);
} else {
  html.classList.remove("dark");
  switchMode.checked = false;
}

///////////////////////////////////////
function handleClickScoll() {
  let BtnscrollTop = $("#iconScroll");
  window.onscroll = (e) => {
    if (body.scrollTop > 120 || browser.scrollTop > 130) {
      BtnscrollTop.classList.replace("invisible", "iconScroll-Top");
    } else {
      BtnscrollTop.classList.replace("iconScroll-Top", "invisible");
    }
  };
  BtnscrollTop.onclick = function () {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };
}
handleClickScoll();
///////////////////////////////////////

let charts = {
  mixedChart() {
    let options = {
      series: [
        {
          name: "Team A",
          type: "column",
          data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30],
        },
        {
          name: "Team Hieu",
          type: "area",
          data: [88, 78, 88, 89, 98, 75, 92, 73, 55, 66, 77],
        },
        {
          name: "Team Thinh",
          type: "line",
          data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
        },
      ],
      colors: ["#00ab55", "#ffc107", "#1890ff"],
      chart: {
        height: 350,
        type: "line",
        stacked: false,
        toolbar: {
          show: false,
        },
      },
      stroke: {
        width: [0, 2, 3],
        curve: "smooth",
      },
      plotOptions: {
        bar: {
          borderRadius: 4,
          columnWidth: "10%",
        },
      },
      fill: {
        opacity: [0.85, 0.1, 1],
        gradient: {
          inverseColors: false,
          shade: "light",
          type: "vertical",
          opacityFrom: 0.85,
          opacityTo: 0.65,
          stops: [0, 100, 100, 100],
        },
      },
      labels: [
        "01/01/2003",
        "02/01/2003",
        "03/01/2003",
        "04/01/2003",
        "05/01/2003",
        "06/01/2003",
        "07/01/2003",
        "08/01/2003",
        "09/01/2003",
        "10/01/2003",
        "11/01/2003",
      ],

      markers: {
        size: 0,
      },
      xaxis: {
        type: "datetime",
      },
      legend: {
        position: "top",
        horizontalAlign: "right",
      },
      yaxis: {
        min: 0,
        max: 100,
        tickAmount: 4,
      },
      tooltip: {
        shared: true,
        intersect: false,
        y: {
          formatter: function (y) {
            if (typeof y !== "undefined") {
              return y.toFixed(0) + " visits";
            }
            return y;
          },
        },
      },
    };
    let chart = new ApexCharts($("#mix-chart"), options);
    chart.render();
  },
  // handle config pieChart
  pieChart() {
    let options = {
      series: [44, 55, 13, 43, 22],
      chart: {
        width: 310,
        type: "pie",
      },
      labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
      responsive: [
        {
          breakpoint: 769,
          options: {
            chart: {
              width: 350,
            },
            legend: {
              position: "right",
            },
          },
        },
        {
          breakpoint: 1025,
          options: {
            chart: {
              width: 350,
            },
            legend: {
              position: "bottom",
            },
          },
        },
        {
          breakpoint: 2000,
          options: {
            chart: {
              width: 350,
            },
            legend: {
              position: "right",
            },
          },
        },
      ],
    };

    const pieChart = new ApexCharts(
      document.querySelector("#pie-chart"),
      options
    );
    pieChart.render();
  },
  // handle config Bar chart
  barChart() {
    const options = {
      series: [
        {
          data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380],
        },
      ],
      chart: {
        type: "bar",
        height: 350,
        toolbar: {
          show: false,
        },
      },
      plotOptions: {
        bar: {
          borderRadius: 4,
          horizontal: true,
          colors: {
            ranges: [
              {
                colors: "rgba(145, 158, 171, 0.24)",
              },
            ],
            backgroundBarColors: "#f64859",
            backgroundBarOpacity: "0",
          },
        },
      },
      colors: ["#0072B5"],
      dataLabels: {
        enabled: true,
        style: {
          colors: ["#fff"],
        },
      },
      xaxis: {
        categories: [
          "South Korea",
          "Canada",
          "United Kingdom",
          "Netherlands",
          "Italy",
          "France",
          "Japan",
          "United States",
          "China",
          "Germany",
        ],
        colors: ["#fff"],
      },
      yaxis: {
        labels: {
          show: true,
        },
      },
      title: {
        text: "Custom DataLabels",
        align: "center",
        floating: true,
      },
      subtitle: {
        text: "Category Names as DataLabels inside bars",
        align: "center",
      },
    };
    const barChart = new ApexCharts($("#bar-chart"), options);
    barChart.render();
  },
  // config radar chart
  radarChart() {
    let options = {
      series: [
        {
          name: "Series 1",
          data: [80, 50, 30, 40, 100, 20],
        },
        {
          name: "Series 2",
          data: [20, 30, 40, 80, 20, 80],
        },
        {
          name: "Series 3",
          data: [44, 76, 78, 13, 43, 10],
        },
      ],
      chart: {
        height: 330,
        type: "radar",
        dropShadow: {
          enabled: true,
          left: 1,
          top: 1,
        },
        toolbar: {
          show: false,
        },
      },
      stroke: {
        width: 2,
      },
      fill: {
        opacity: 0.1,
      },
      markers: {
        size: 0,
      },
      xaxis: {
        categories: [
          "English",
          "History",
          "Physics",
          "Geography",
          "Chinese",
          "Math",
        ],
      },
    };

    var chart = new ApexCharts($("#radar-chart"), options);
    chart.render();
  },
};
const handleTasks = function () {
  let check = $$(".task_input-check");
  const inputLength = check.length;
  const formChecks = $$(".task");
  for (let i = 0; i < inputLength; i++) {
    formChecks[i].addEventListener("click", function (e) {
      console.log($(".done"));
      check[i].checked
        ? formChecks[i].classList.add("done")
        : formChecks[i].classList.remove("done");
    });
  }
};
var dataUser = "http://localhost:3000/users";

const handleAPI = function (dataUrl) {
  return {
    async request() {
      let options = { method: "GET" };
      const response = await fetch(dataUrl, options);
      const json = await response.json();
      return json;
    },
    addUser(data) {
      async function post(data, callback) {
        let options = {
          method: "POST",
          headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
          body: JSON.stringify(data),
        };
        const response = await fetch(data, options);
        let json = await response.json();
      }
      post(data);
    },
  };
};
const handleRenderInfUsers = (() => {
  const Table = $("Table");
  let newUser = {};
  const dataForm = $(".post");
  let inputs = dataForm.querySelectorAll("[name]:not(meta)");
  console.log(inputs);
  const btnSubmitAddUser = $(".addUser-submit");
  return {
    render(data) {
      const dataTable = data.map((data, index) => {
        return Html`<tr class=" border-b dark:border-textDark hover:bg-[#919eab0f] ">
          <td class=""><input type="checkbox" name="" id="" /></td>
          <td class="">
            <div class="base-users flex-start">
              <img
                src="${data.avatar}"
                onerror="this.src='../img/errorImg.png'"
                class=" w-9 h-9 align-middle rounded-circle "
                alt=""
              />
              <h5
                class="font-medium ml-2 text-center text-lg"
              > 
                ${data.nameUser}
              </h5>
            </div>
          </td>
          <td class="text-base font-normal">${data.company}</td>
          <td class="">${data.role.map(
            (rule) => Html`<span class="text-sm">${rule}  </span>`
          )}</td>
          <td class="text-base">Yes</td>
          <td class=""> 
            <button onclick="btnSubmitAddUser.activeUser(${data.status})" id="status-user" class="text-base ${
              (data.status && "active") || "offline"
            } w-16 rounded-4xl  h-5">${
          (data.status && "Active") || "Offline"
        } </button>
          </td>
          <td class="flex-center">
          <button class="active:bg-[#919eab0f] rounded-sm1 w-6 h-10">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z"
              />
            </svg>
            </button>
          </td>
        </tr>`;
      });
      Table.insertAdjacentHTML("beforeend", dataTable);
    },
    activeUser(data){
      data = !data;
    },
    updateData() {
      let newDataPost = new Map();
      for (let input of [...inputs]) {
        if (input.value != false) {
          newUser[input.name] = input.value;
          if (input.name == "role") {
            newUser[input.name] = [input.value.split(",")];
          }
          input.name == "Post" &&
            (newDataPost["content"] = input.name == "Post" && input.value);
          input.name == "linkImage" &&
            (newDataPost["imageBlock"] =
              input.name == "linkImage" && input.value);
        }
      }
      newUser["blogs"] = newDataPost;

      return newUser;
    },
    init(dataUser) {
      async function request() {
        const dataSource = await handleAPI(dataUser).request();
        await handleRenderInfUsers.addUser(dataSource);
        await handleRenderInfUsers.render(dataSource);
        await handleRenderInfUsers.activeUser()
      }
      request();
    },
    addUser(dataUser) {
      btnSubmitAddUser.onclick = (e) => {
        e.preventDefault();
        const newData = this.updateData();
        console.log(newData);
        this.render([newData]);
        handleAPI(dataUser).addUser(newData);
      };
    },
  };
})();

handleTasks();
handleRenderInfUsers.init(dataUser);
charts.mixedChart();
charts.pieChart();
charts.barChart();
charts.radarChart();
