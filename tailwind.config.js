module.exports = {
  purge: ["./**/*.html"],
  darkMode: "class", // or 'media' or 'class'
  mode: "jit",
  theme: {
    colors: {
      black: "#000",
      white: "#fff",
      blue: "#0072B5",
      xam: "#f4f6f8",
      overlay: "rgba(22, 28, 36, 0.48)",
      red: "#f64859",
      danger: "#dc3545",
      bgDarkMode: "#212b36",
      textDark: "#637381",
      colorBorder: "rgba(145, 158, 171, 0.24)",
      sideBarDark: "rgba(145, 158, 171, 0.06)",
      pink: "#ee9ca7",
      hue: "#ffdde1",
    },
    borderRadius: {
      circle: "50%",
    },
    fontFamily: {
      sans: ["Graphik", "sans-serif"],
      serif: ["Merriweather", "serif"],
    },
    extend: {
      keyframes: {
        flad: {
          from: { opacity: "1" },
          to: { opacity: "0" },
        },
      },
      animation: {
        "fald-in": "flad 0.2s linear",
      },
      spacing: {
        128: "32rem",
        144: "36rem",
        nav: "280px",
        heightHeader: "90px",
        57: "240px",
        17: "72px",
        0.3: "0.4px",
        55: "220px",
        "1/5": "20%",
        "1/6": "16.667%"
      },  
      borderRadius: {
        "4xl": "1rem",
        sm1: "8px",
      },
      screens: {
        desktop: "1224px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
};
